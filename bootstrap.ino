//Erik Pope (Agent6)
//9/5/1018
//Bootstrap Template
#include <Arduboy2.h>
Arduboy2 arduboy;

enum class GameState : uint8_t
{
  stateMenuIntro,
  stateMenuMain,
  stateMenuHelp,
  stateMenuPlay,
  stateGamePlaying,
  stateGameOver,
};

GameState currentState = GameState::stateMenuIntro;

void changeState(GameState state)
{
  currentState = state;
}

void stateMenuIntro(){
  if(arduboy.justPressed(A_BUTTON))
  changeState(GameState::stateMenuMain);
  
  arduboy.setCursor(8, 56);
  arduboy.print(F("Press A -> MenuMain"));  
  arduboy.setCursor(16, 24);
  arduboy.print(F("You are in the"));
  arduboy.setCursor(16, 32);
  arduboy.print(F("stateMenuIntro"));
}

void stateMenuMain(){
  if(arduboy.justPressed(A_BUTTON))
  changeState(GameState::stateMenuHelp);
  
  arduboy.setCursor(8, 56);
  arduboy.print(F("Press A -> MenuHelp"));
  arduboy.setCursor(16, 24);
  arduboy.print(F("You are in the"));
  arduboy.setCursor(16, 32);
  arduboy.print(F("stateMenuMain"));
}

void stateMenuHelp(){
  if(arduboy.justPressed(A_BUTTON))
  changeState(GameState::stateMenuPlay);
  
  arduboy.setCursor(8, 56);
  arduboy.print(F("Press A -> MenuPlay"));
  arduboy.setCursor(16, 24);
  arduboy.print(F("You are in the"));
  arduboy.setCursor(16, 32);
  arduboy.print(F("stateMenuHelp"));
}

void stateMenuPlay(){
  if(arduboy.justPressed(A_BUTTON))
  changeState(GameState::stateGamePlaying);
  
  arduboy.setCursor(8, 56);
  arduboy.print(F("Press A -> GamePlaying"));
  arduboy.setCursor(16, 24);
  arduboy.print(F("You are in the"));
  arduboy.setCursor(16, 32);
  arduboy.print(F("stateMenuPlay"));
}

void stateGamePlaying(){
  if(arduboy.justPressed(A_BUTTON))
  changeState(GameState::stateGameOver);
  
  arduboy.setCursor(8, 56);
  arduboy.print(F("Press A -> GameOver"));
  arduboy.setCursor(16, 24);
  arduboy.print(F("You are in the"));
  arduboy.setCursor(16, 32);
  arduboy.print(F("stateGamePlaying")); 
}

void stateGameOver(){
  if(arduboy.justPressed(A_BUTTON))
  changeState(GameState::stateMenuIntro);
  
  arduboy.setCursor(8, 56);
  arduboy.print(F("Press A -> MenuIntro"));
  arduboy.setCursor(16, 24);
  arduboy.print(F("You are in the"));
  arduboy.setCursor(16, 32);
  arduboy.print(F("stateGameOver"));
}

void setup() {
    arduboy.begin();
    arduboy.clear();
    arduboy.setFrameRate(60);
}

void loop() {
  if(!arduboy.nextFrame())
    return;
    
  arduboy.pollButtons();
  arduboy.clear();
  
  switch(currentState)
  {
    case GameState::stateMenuIntro:
      stateMenuIntro();
      break;
    case GameState::stateMenuMain:
      stateMenuMain();
      break;
    case GameState::stateMenuHelp:
      stateMenuHelp();
      break;
    case GameState::stateMenuPlay:
      stateMenuPlay();
      break;
    case GameState::stateGamePlaying:
      stateGamePlaying();
      break;
    case GameState::stateGameOver:
      stateGameOver();
      break;

  }
  
  arduboy.display();
}